import logo from './logo.svg';

function App() {
  return (
    <div className="App">
      <header className="App-header">
        
        <p style={{ textAlign: 'right', marginRight: 'auto', marginLeft: 'auto' }}>
          VAMK ON #PARASTAAIKAA 
        </p>
        
        <a
          className="App-link"
          href="https://reactjs.org"
          target="_blank"
          rel="noopener noreferrer"
        >
          
        </a>
      </header>
    </div>
  );
}

export default App;

